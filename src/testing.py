from src.utils import load_models_f


def clean_answer(a: str) -> str:
    return a.split("<|endoftext|>")[0]


print("Welcome to the testing field !")

gpt2 = load_models_f(max)

questions = [
    "Last of array",
    "I want the 3rd element",
    "Give me the 3rd element",
    "The element at position 10",
    "Give me from the 5th to the 10th with steps of 3",
    "Inverse",
    "Get all elements from start to 50th in a list",
]

for q in questions:
    print(f"[Question] {q}")
    q = f"{q}<SEP>"
    answer = gpt2.predict(q)[len(q) :]
    answer = clean_answer(answer)
    print(f"[Answer] {answer}")
    print("=" * 50)
