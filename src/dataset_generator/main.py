import contextlib
import multiprocessing
import os
import shutil
from concurrent.futures import ProcessPoolExecutor
from copy import deepcopy
from io import StringIO
from pathlib import Path

import pandas as pd

from src.dataset_generator.augmenter_conf import AUG_PARAMS
from src.dataset_generator.data_augmenter import DataAugmenter

# Params
from src.utils import load_models_f

WORKERS = multiprocessing.cpu_count() // 2
SIZE = 1_000_000 // 2

# Dataset
SOURCE_NAME = "dataset.csv"
AUGMENTATION_SOURCE_PATH = Path("augmentation_sources", SOURCE_NAME)
DATASET = pd.read_csv(AUGMENTATION_SOURCE_PATH)
OUTPUT_DATASET_NAME = f"DATASET_{SIZE}.txt"
OUTPUT_DATASET_PATH = Path("dataset", OUTPUT_DATASET_NAME)

# Models
gpt2 = load_models_f(min)
data_augmenter = DataAugmenter(
    gpt2.tokenizer, DATASET["x"], DATASET["y"], augmenter_params=AUG_PARAMS
)


def generate_part(data_augmenter_: DataAugmenter, max_i: int) -> StringIO:
    buffer = StringIO()
    for i in range(max_i):
        X, y = data_augmenter_.get_random_item(encode=False)
        buffer.write(f"{X}<SEP>{y}{gpt2.tokenizer.eos_token}")
    return buffer


with contextlib.suppress(OSError):
    os.remove(OUTPUT_DATASET_PATH)

with open(OUTPUT_DATASET_PATH, "a+") as f:

    def callback(future):
        buffer = future.result()
        buffer.seek(0)
        shutil.copyfileobj(buffer, f)

    batch_size = SIZE // WORKERS
    with ProcessPoolExecutor(max_workers=WORKERS) as executor:
        for _ in range(WORKERS):
            executor.submit(
                generate_part, deepcopy(data_augmenter), batch_size
            ).add_done_callback(callback)
