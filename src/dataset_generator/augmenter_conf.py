import nlpaug.augmenter.char as nac

from src.dataset_generator.custom_augmenters.qwerty_to_azerty import QwertyToAzertyAug
from src.dataset_generator.custom_augmenters.remove_hyphens import RemoveHyphensAug
from src.dataset_generator.custom_augmenters.remove_whitespaces import (
    RemoveWhitespacesAug,
)

AUG_PARAMS = [
    nac.RandomCharAug(action="delete"),
    nac.RandomCharAug(action="insert"),
    nac.RandomCharAug(),
    RemoveWhitespacesAug(p=0.1),
    RemoveHyphensAug(p=0.1),
    QwertyToAzertyAug(p=0.1),
]
