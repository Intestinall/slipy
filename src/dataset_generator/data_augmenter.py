import random
from dataclasses import dataclass
from typing import List, Tuple, Union

import nlpaug.flow as naf
import numpy as np
from tensorflow.python.keras.utils.data_utils import Sequence
from transformers import GPT2Tokenizer

from src.dataset_generator.num2words_enum import Num2Words


@dataclass
class DataAugmenter(Sequence):
    """
    X : Natural language sentences
    y : Python equivalent code
    z : Number of index in python code
    """

    tokenizer: GPT2Tokenizer
    X: List
    y: List
    augmenter_params: List
    start: int = -100
    stop: int = 100

    def __post_init__(self) -> None:
        if len(self.X) != len(self.y):
            raise Exception("X and y must have the same length.")

        self.z = [x.count("{}") for x in self.X]

        self.augmenter = naf.Sometimes(self.augmenter_params)

    def __len__(self) -> int:
        return len(self.X)

    def shuffle_augmenters(self) -> None:
        random.shuffle(self.augmenter)

    def _augment(self, X: str) -> str:
        # TODO: maybe n parameters can be useful for performances
        return self.augmenter.augment(X)

    def _fill_X(self, X: str, z_numbers: List[int]) -> str:
        nums = [Num2Words.to_str(zz) for zz in z_numbers]
        return X.format(*nums)

    def _fill_y(self, y: str, z_numbers: List[int]) -> str:
        return y.format(*z_numbers)

    def _generate_z_numbers(self, z: int) -> List[int]:
        return random.choices(range(self.start, self.stop), k=z)

    def _encode(self, data: str) -> np.ndarray:
        return np.asarray(self.tokenizer.encode(data))

    def _random_idx(self) -> int:
        return random.randint(0, len(self.X) - 1)

    def get_x_y_z(self) -> Tuple[str, str, int]:
        idx = self._random_idx()
        return self.X[idx], self.y[idx], self.z[idx]

    def get_random_item(
        self, encode: bool = True
    ) -> Union[Tuple[str, str], Tuple[np.ndarray, np.ndarray]]:
        self.shuffle_augmenters()
        X, y, z = self.get_x_y_z()
        z_numbers = self._generate_z_numbers(z)

        X = self._augment(self._fill_X(X, z_numbers))
        y = self._fill_y(y, z_numbers)
        if encode:
            return self._encode(X), self._encode(y)
        else:
            return X, y
