from src.dataset_generator.custom_augmenters.base import BaseCustomAug


class RemoveWhitespacesAug(BaseCustomAug):
    def transform(self, data: str) -> str:
        return "".join([c for c in data if not c.isspace() or self.must_edit()])
