from src.dataset_generator.custom_augmenters.base import BaseCustomAug


class QwertyToAzertyAug(BaseCustomAug):
    TRANSLATION = str.maketrans("qwerty", "azerty")

    def _translate(self, c: str) -> str:
        return c.translate(QwertyToAzertyAug.TRANSLATION) if self.must_edit() else c

    def transform(self, data):
        return "".join([self._translate(c) for c in data])
