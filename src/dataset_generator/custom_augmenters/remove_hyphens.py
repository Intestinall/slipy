from src.dataset_generator.custom_augmenters.base import BaseCustomAug


class RemoveHyphensAug(BaseCustomAug):
    def transform(self, data: str) -> str:
        return "".join([c for c in data if c != "-" or self.must_edit()])
