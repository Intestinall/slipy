import random

from nlpaug.augmenter.char import CharAugmenter


class BaseCustomAug(CharAugmenter):
    def __init__(
        self,
        p: float,
        name="CustomAugmenter",
        min_char=2,
        aug_char_min=1,
        aug_char_max=10,
        aug_char_p=0.2,
        aug_word_min=1,
        aug_word_max=10,
        aug_word_p=0.3,
        tokenizer=None,
        reverse_tokenizer=None,
        stopwords=None,
        verbose=0,
        stopwords_regex=None,
        device="cpu",
    ):
        super().__init__(
            name=f"{name}{self.__class__.__name__}",
            action="delete",
            min_char=min_char,
            aug_char_min=aug_char_min,
            aug_char_max=aug_char_max,
            aug_char_p=aug_char_p,
            aug_word_min=aug_word_min,
            aug_word_max=aug_word_max,
            aug_word_p=aug_word_p,
            tokenizer=tokenizer,
            reverse_tokenizer=reverse_tokenizer,
            stopwords=stopwords,
            device=device,
            verbose=verbose,
            stopwords_regex=stopwords_regex,
        )
        self.p = p

    def delete(self, data):
        return self.transform(data)

    def must_edit(self) -> bool:
        return random.random() < self.p

    def transform(self, data: str) -> str:
        raise NotImplementedError()
