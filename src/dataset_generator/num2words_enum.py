import random
from enum import Enum

from num2words import num2words


class Num2Words(Enum):
    ORDINAL_NUM = "ordinal_num"
    ORDINAL = "ordinal"
    CARDINAL = "cardinal"
    CARDINAL_NUM = None

    @staticmethod
    def _random() -> "Num2Words":
        return random.choice(list(Num2Words))

    def _is_ordinal(self) -> bool:
        return self in (Num2Words.ORDINAL, Num2Words.ORDINAL_NUM)

    def _is_numeral(self) -> bool:
        return self in (Num2Words.ORDINAL_NUM, Num2Words.CARDINAL_NUM)

    def add_minus(self, number: str) -> str:
        if self._is_numeral():
            return f"-{number}"
        else:
            # TODO : "from the end" would be more accurate
            return f"minus {number}"

    def convert(self, number: int) -> str:
        """
        Decrement number if ordinal because :
            - Element 1 => [1]
            - 1th element => [0]
        """
        if self is not Num2Words.CARDINAL_NUM:
            return num2words(
                number - 1 if self._is_ordinal() and number != 0 else number,
                to=self.value,
            )
        else:
            return str(number)

    @staticmethod
    def to_str(number: int) -> str:
        is_minus = number < 0
        number = abs(number)
        converter = Num2Words._random()
        number = converter.convert(number)
        return converter.add_minus(number) if is_minus else number
