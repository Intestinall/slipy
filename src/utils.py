import os
from pathlib import Path
from typing import Callable

import tensorflow as tf

from src.model import MyGpt2


def get_dataset(
    dataset_path: Path,
    gpt2_,
    block_size: int = 100,
    batch_size: int = 12,
    buffer_size: int = 1000,
) -> tf.data.Dataset:
    with open(Path("dataset", dataset_path)) as f:
        string_tokenized = gpt2_.tokenizer.encode(f.read())

    examples = []
    for i in range(0, len(string_tokenized) - block_size + 1, block_size):
        examples.append(string_tokenized[i : i + block_size])
        examples.append(string_tokenized[i : i + block_size])

    inputs, labels = [], []
    for ex in examples:
        inputs.append(ex[:-1])
        labels.append(ex[1:])

    dataset = tf.data.Dataset.from_tensor_slices((inputs, labels))
    return dataset.shuffle(buffer_size).batch(batch_size, drop_remainder=True)


def load_models_f(f: Callable) -> MyGpt2:
    first_save = f(os.listdir("model_save"))
    p = Path("model_save", first_save).resolve()
    return MyGpt2.load(p)
