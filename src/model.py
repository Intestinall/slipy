import os
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path
from typing import Union

import tensorflow as tf
from transformers import CONFIG_NAME, GPT2Tokenizer, TFGPT2LMHeadModel

gpus = tf.config.experimental.list_physical_devices("GPU")
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)


@dataclass
class MyGpt2:
    tokenizer_from: Union[str, Path] = "gpt2"
    model_from: Union[str, Path] = "gpt2"
    from_pt: bool = False

    def __post_init__(self) -> None:
        self.tokenizer = GPT2Tokenizer.from_pretrained(
            self.tokenizer_from, from_pt=self.from_pt
        )
        self.model = TFGPT2LMHeadModel.from_pretrained(
            self.model_from, return_dict=True, from_pt=self.from_pt
        )
        self._compile()

    def predict(self, data: str) -> str:
        inputs = self.tokenizer.encode(data, return_tensors="tf")
        outputs = self.model.generate(
            inputs,
            max_length=50,
            num_beams=5,
            temperature=0.7,
            no_repeat_ngram_size=2,
            num_return_sequences=5,
            pad_token_id=50256,
        )
        return self.tokenizer.decode(outputs[0])

    def fit(self, *args, **kwargs) -> None:
        return self.model.fit(*args, **kwargs)

    def _compile(self) -> None:
        optimizer = tf.keras.optimizers.Adam(
            learning_rate=3e-5, epsilon=1e-08, clipnorm=1.0
        )
        loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
        metric = tf.keras.metrics.SparseCategoricalAccuracy("accuracy")
        self.model.compile(
            optimizer=optimizer,
            loss=[loss, *[None] * self.model.config.n_layer],
            metrics=[metric],
        )

    def save(self) -> None:
        now = datetime.now()
        output_dir = Path("model_save", now.strftime("%Y_%m_%dT%H_%M_%S"))
        output_dir.mkdir(parents=True, exist_ok=True)
        model_to_save = (
            self.model.module if hasattr(self.model, "module") else self.model
        )
        output_config_file = os.path.join(output_dir, CONFIG_NAME)
        self.model.save_pretrained(output_dir)
        self.tokenizer.save_pretrained(output_dir)
        model_to_save.config.to_json_file(output_config_file)

    @classmethod
    def load(cls, path: Path) -> "MyGpt2":
        return cls(path, path)
