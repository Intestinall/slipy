from pathlib import Path

from src.utils import get_dataset, load_models_f

DATASET_NAME = Path("DATASET_500000.txt")

if __name__ == "__main__":
    gpt2 = load_models_f(max)
    dataset = get_dataset(DATASET_NAME, gpt2)
    gpt2.fit(dataset, epochs=3)
    gpt2.save()
