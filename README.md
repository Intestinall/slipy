# Slipy


### Filling ([num2words](https://github.com/savoirfairelinux/num2words))
- Number to ordinal num.
- Number to ordinal.
- Number to cardinal.

### Augmentations ([nlpaug](https://github.com/makcedward/nlpaug))
- Synonyms (tokenize them).
- Add noise.
- Remove some words (random).
- Retro translation.
- OCR augmenter.
- keyboard augmenter.
- split augmenter.

For more examples, see : https://github.com/makcedward/nlpaug/blob/master/example/textual_augmenter.ipynb

### Custom augmentations
- Remove hyphens : `str.replace("-")`
- Remove some whitespaces (random) : `str.replace()`
- qwerty-azerty : `str.maketrans('qwerty', 'azerty')`


### TODO
- Maybe n parameters (for `.augment()` method) can be useful for performances.
