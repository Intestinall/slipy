.PHONY: format

format:
	python -m isort src/ && python -m black src/ && python -m flake8 --max-line-length=88 src/
